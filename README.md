# Username Reader

### Description

**About this program, what's it do?**

A simple console application to read usernames out of a file, and list add or remove each one.

The application has two modes, one when invoked from a console with command line arguments and another when run without. When run alone it enters interactive mode and asks for input regarding options. These options can also be specified in argument format for running in scripts or simply if that method of execution is preferred. When outside of interactive mode the argument format is as follows:

```
argument 1: action [list/add/remove]

argument 2: action specific option [filter mode, name to add/remove, etc. use "help" to list the ones available for the current action]

argument 3 (optional): A file path to override the default Gamertags.txt. this can be used to specify any arbitrary file to read or write to.
```

This structure is similar to interactive mode, however it walks the user through each option individually.

The project comes with two demo files to read. Gamertags.txt with a variety of names perfect for filtering, and normalnames.txt which I used mainly to test the alternate file paths feature.

### Technical stuff

**The nerdy bit of the README For if you're interested in the code. Plus I just really like talking about code and you can't stop me this is my project I can do whatever I want.**

**This program and its source code is available as a public git repository at [https://codeberg.org/nullbyte/UsernameReader](https://codeberg.org/nullbyte/UsernameReader)**

This was written in C# using the .NET framework 6.0, so make sure you have that installed and set up before attempting to build or run this project.

This application is split into 3 different segments.

`Program.cs` for initialisation and getting user input

`UsernameReader.cs` for interacting with the files, reading and writing the data

`Actions.cs` for taking data from UsernameReader and processing it for output (or vice versa). This is what manages the bulk of the work.

As said before, when run without any arguments the program enters interactive mode.

Internally interactive mode generates a set of arguments which are then treated exactly the same as when they are specified manually. This is then passed to the desired action which processes the request along with any options given. Each action is represented as a class that derives from `Action`, and to be made available to the program it must be added to the global `actions` variable. This should be enough with the exception of interactive mode, which has its own array which would need to be updated, as well as some additional help text to be displayed.
