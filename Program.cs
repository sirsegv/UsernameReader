﻿using System;

namespace ICTPRG302_Intro_to_Programming {
	class Program {

		// List of valid actions and instances of each action class to use. Polymorphism is cool
		static Dictionary<string, Action> actions = new Dictionary<string, Action>() {
			{"list", new ListNames()},
			{"add", new AddName()},
			{"remove", new RemoveName()}
		};

		/// <summary>
		/// Program entry point
		/// </summary>
		static void Main(string[] args) {
			
			// Determines if the program should run in interactive mode rather than getting input from command like arguments
			var interactive = false;
			
			// Argument structure:
			// args[0]: action (list, add, remove)
			// args[1]: action specific option (filter mode, name, etc.)
			// args[2]: optional alternate file path (overrides Gamertags.txt)

			// If no arguments specified, run in interactive mode
			if (args.Length == 0) {
				interactive = true;
				args = GetInput();
			}

			if (args.Length > 0 && actions.Keys.Contains(args[0])) {
				// Check if user is requesting help for action
				if (args.Length == 1 || (args.Length > 1 && args[1] == "help")) {
					// Display action specific help
					actions[args[0]].PrintHelp();
				} else {
					// Create FileReader object
					var fileReader = new FileReader();
					
					// Attempt to load usernames based on last argument if it exists
					var namesloaded = fileReader.LoadUsernames(args.Length > 2 ? args[2] : "");
					// Checks if names have successfully loaded
					if (namesloaded) {
						// Runs specified action and pass it the names reader (to access and save usernames) and arguments which contain action options
						actions[args[0]].LoadFileReader(fileReader);
						actions[args[0]].Run(args);
					}
				}
			} else {
				// Show global help if invalid action entered
				ShowHelp();
			}

			// Prevent console from closing when executed through Windows
			if (OperatingSystem.IsWindows() && interactive) {
				Console.WriteLine("");
				Console.Write("Press any key to exit");
				Console.ReadKey();
			}
		}

		/// <summary>
		/// General help message detailing program operation
		/// </summary>
		static void ShowHelp(bool interactive = false) {
			// Only show the command structure when running from a command prompt
			if (!interactive) {
				Console.WriteLine("Usage: NamesReader [ACTION] [OPTIONS] [PATH]");
				Console.WriteLine("  Run NamesReader [ACTION] help for action specific help");
				Console.WriteLine("");
			} else {
				Console.WriteLine("This program is running in interactive mode, to run outside of interactive mode please provide command line arguments");
			}
			Console.WriteLine("  Actions:");
			Console.WriteLine("    list: Print all usernames");
			Console.WriteLine("    add: Add new username");
			Console.WriteLine("    remove: Remove username");
			
			// In interactive mode we don't need to tell the user about this as it's already shown when they are prompted in later questions
			if (!interactive) {
				Console.WriteLine("");
				Console.WriteLine("  Options:");
				Console.WriteLine("    See action specific help");
				Console.WriteLine("");
				Console.WriteLine("  Path:");
				Console.WriteLine("    Alternate file path to search (defaults to Gamertags.txt)");
			}
		}

		/// <summary>
		/// Gets user input and returns an array that follows the same guidelines as the command line arguments, intended to be used in situations where entering command line arguments is not easy, such as when running under Windows. Cross Platform development is hard :(
		/// </summary>
		// I wrote this code at midnight after our first lesson so it might not be the most maintainable thing in the world
		static string[] GetInput() {
			// Array to simulate command line arguments from user input
			var emulatedArgs = new string[3];
			// Variable for user input
			string? input = null; // Nullable variables aren't good for program safety, but they're so so useful...

			// Help text for each stage of input
			var messages = new string[] {"Which action do you want to take? [list/add/remove]: ", "[PLACEHOLDER]", "Which file do you want to read? [Default: Gamertags.txt]: "};
			// Help text for individual actions
			var actionmessages = new Dictionary<string, string>() {{"list", "Which filter mode do you want to use? [0-5]: "},
																	{"add", "Which name would you like to add?: "},
																	{"remove", "Which name would you like to remove?: [name or number in list]: "}};
			
			Console.WriteLine("Type \"help\" at any point for more information");
			
			// Holds current iteration
			int i = 0;
			// Iterate through questions
			foreach (string message in messages) {
				
				// Check for input and loop if the user asks for help
				while (input == null || input == "help") {
					Console.Write(message);
					input = Console.ReadLine();
					// Get context sensitive help
					if (input == "help") {
						if (i == 0) {
							// If no action has been set display global help
							ShowHelp(true);
						} else {
							// Else show action specific help
							actions[emulatedArgs[0]].PrintHelp();
						}
					}
					
					// Perform validity checks depending on the current question
					if (i == 0 && !(input == "help")) {
						if (!(actionmessages.Keys.Contains(input?.ToLower()) || String.IsNullOrEmpty(input))) {
							// If input isn't a valid action loop again
							Console.WriteLine("Not a valid action");
							input = null;
						} else {
							// If input is valid or null
							if (String.IsNullOrEmpty(input)) {
								// If input is null default to list
								Console.WriteLine("No action specified, defaulting to list");
								input = "list";
							}

							// Make input lowercase
							input = input?.ToLower();

							// Update help text for next item
							messages[1] = actionmessages[input?.ToLower()!];
						}
					} else if (i == 1) {
						if (emulatedArgs[0] != "list" && String.IsNullOrEmpty(input)) {
							// If the user is adding or removing a username, make sure they actually gave a username
							Console.WriteLine("Please specify a username to " + emulatedArgs[0]); // Fancy coding UwU
							input = null;
						}
					}
				}

				if (input != null) {
					// If the input was valid, update emulated arguments list, reset input and continue
					emulatedArgs[i] = input;
					input = null;
					i++;
				}
			}
			
			// Return list of arguments to use
			return emulatedArgs;
		}
	}
}
