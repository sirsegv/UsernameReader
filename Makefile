run:
	@dotnet run

build:
	@dotnet build

clean:
	@rm -r bin obj

linux:
	dotnet publish -c Release -r linux-x64 -p:PublishReadyToRun=true -p:PublishSingleFile=true --self-contained

windows:
	dotnet publish -c Release -r win-x64 -p:PublishReadyToRun=true -p:PublishSingleFile=true --no-self-contained

linuxaot:
	sed -i -e 's/net6.0/net7.0/g' UsernameReader.csproj
	dotnet publish -c Release -r linux-x64 -p:PublishAot=true -p:StripSymbols=true
	sed -i -e 's/net7.0/net6.0/g' UsernameReader.csproj